This example briefly demonstrates how to use JCLTP, JCLAL_TEXT and JCLAL.

## Table of contents

- [Create a Weka dataset](#create-weka-dataset)
- [Transform the dataset](#transform-the-dataset)
- [Check the transformed dataset](#check-the-transformed-dataset)
- [Execute a JCLAL's experiment on the new dataset](#execute-a-experiment-using-jclal)

### Create a Weka dataset

In the Java file [CreateWekaDataset.java](src/net/sf/jclal/examples/CreateWekaDataset.java) it is shown an example demonstrating how to create and save a dataset containing text instances.

### Transform the dataset

After you have saved the dataset containing the instances with the raw texts, you can configure the so-called StringToWordVector filter provided by Weka as is indicated in the figure. Then, 
you can save to your local directory the new transformed dataset.

<img alt="" src="./help/filter01.jpg">

### Check the transformed dataset

First, execute the GUI Explorer of Weka and open the transformed dataset.  Second, set the class of the dataset to the first attribute named @@class@@.

<img alt="" src="./help/dataset01.jpg">

Then, you can apply any classification model provided by Weka as is portrayed in the following figure.

<img alt="" src="./help/dataset02.jpg">

### Execute a experiment using JCLAL

Now you have the transformed dataset, you can apply an active learning method with the [JCLAL](https://github.com/ogreyesp/JCLAL) framework. The easier way to use JCLAL is by configuring an experiment with an XML file. You can reuse the examples that are provided by JCLAL, in this case we selected the example 
[HoldOut](https://github.com/ogreyesp/JCLAL/blob/master/examples/SingleLabel/HoldOut/EntropySamplingQueryStrategy.cfg) which represents a classical training procedure by using a hold-out evaluation method.

It is noteworthy that if you use the transformed dataset, you must specify the index of the class attribute; by default, JCLAL framework considers that the last attribute of the dataset is the class. 
Let us say that the name of the transformed datasets is `output.arff`, so the XML configuration file would look as follows:
```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<experiment>
    <process evaluation-method-type="net.sf.jclal.evaluation.method.HoldOut">
        ...
        <file-dataset>datasets/output.arff</file-dataset>
        <class-attribute>0</class-attribute>
        <percentage-split>66</percentage-split>
        ...
</experiment>

```
Now, you are ready to use JCLAL on the NLP task. Note that, JCLAL provides a rich set of interfaces that allows you to take the control of almost everything involved in the induction process of a predictive model. 
You can consult the documentation of JCLAL to see how to extend the framework to the user's needs.

