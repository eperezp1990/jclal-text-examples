/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.sf.jclal.examples;

import net.sf.jclal.text.ActiveLearningRanking;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

import java.io.File;
import java.nio.file.Files;

/**
 * Example of JCLAL text.
 *
 * @author Oscar Gabriel Reyes Pupo
 * @author Eduardo Perez Perdomo
 */
public class CreateWekaDataset {
    public static void main(String[] args) throws Exception {
        ActiveLearningRanking alr = new ActiveLearningRanking();

        String[] classes = new String[]{"sports", "medicine"};

        //prepare filters
        //the filter can be created or modified using the weka user interface
        //in Knowledge Flow GUI > filters > unsupervised > attribute
        // the filter used in this case is StringToWordVector
        alr.setFilterPath("config/filterWeka");
        alr.configTextProcess();

        alr.addInstance("\n" +
                        "Sport in childhood. Association football, shown above, is a team sport which also provides " +
                        "opportunities to nurture physical fitness and social interaction skills.\n" +
                        "Sport (British English) or sports (American English) includes all forms of " +
                        "competitive physical activity or games which,[1] through casual or organised " +
                        "participation, aim to use, maintain or improve physical ability and skills " +
                        "while providing enjoyment to participants, and in some cases, entertainment " +
                        "for spectators.[2] Usually the contest or game is between two sides, each attempting " +
                        "to exceed the other. Some sports allow a tie game; others provide tie-breaking methods, to " +
                        "ensure one winner and one loser. A number of such two-sided contests may be arranged in a " +
                        "tournament producing a champion. Many sports leagues make an annual champion by arranging games " +
                        "in a regular sports season, followed in some cases by playoffs. Hundreds of sports exist, from " +
                        "those between single contestants, through to those with hundreds of simultaneous participants, " +
                        "either in teams or competing as individuals. In certain sports such as racing, many contestants " +
                        "may compete, each against each other, with one winner.",
                "wiki1", classes[0]);

        alr.addInstance("Sports are usually governed by a set of rules or customs, which serve to ensure fair " +
                        "competition, and allow consistent adjudication of the winner. Winning can be determined by physical " +
                        "events such as scoring goals or crossing a line first. It can also be determined by judges who are " +
                        "scoring elements of the sporting performance, including objective or subjective measures such as technical " +
                        "performance or artistic impression.",
                "wiki2", classes[0]);

        alr.addInstance("Medicine is the science and practice of the diagnosis, treatment, and prevention of " +
                        "disease. Medicine encompasses a variety of health care practices evolved to maintain and restore health " +
                        "by the prevention and treatment of illness. Contemporary medicine applies biomedical sciences, biomedical " +
                        "research, genetics, and medical technology to diagnose, treat, and prevent injury and disease, typically " +
                        "through pharmaceuticals or surgery, but also through therapies as diverse as psychotherapy, external splints " +
                        "and traction, medical devices, biologics, and ionizing radiation, amongst others.",
                "wiki3", classes[1]);
        alr.addInstance("The medical decision-making (MDM) process involves analysis and synthesis of all the above " +
                        "data to come up with a list of possible diagnoses (the differential diagnoses), along with an idea of what " +
                        "needs to be done to obtain a definitive diagnosis that would explain the patient's problem",
                "wiki4", classes[1]);

        //add many more instances and classes.........................

        //apply filters and create the weka dataset
        Instances data = alr.createWekaDataset();

        //now you already have a dataset in the weka format and you can apply the JCLAL framework at your convenience

        //store dataset in hard drive
        ArffSaver saved = new ArffSaver();
        saved.setInstances(data);
        //verify that the file output.arff does not exist in the root of the project
        File save = new File("output.arff");
        if (save.exists()) {
            save.delete();
        }
        saved.setDestination(Files.newOutputStream(save.toPath()));
        saved.writeBatch();

        //Please note: the attribute that defines the dataset class is the first one
        //It is not the last as is common in weka datasets

        System.out.println("Process finished, please check the dataset > output.arff ");

    }
}
